import uv1 from 'uuid/v1';
import {
    isArray,
    isObject,
    isNotNullAndNotEmpty
} from './checker';
import db from "./mysql";
import {ResponseSaveError, ResponseSaveSuccess} from "./response";

Date.prototype.addHours = function (h) {
    this.setHours(this.getHours() + h);
    return this;
};

export const get_created_at = () => {
    const d = new Date().addHours(7);
    const year = d.getFullYear();
    let month = d.getMonth() + 1;
    let day = d.getDate();
    let hour = d.getHours();
    let min = d.getMinutes();
    let sec = d.getSeconds();
    month = month.toString().length > 1 ? month : `0${month}`;
    day = day.toString().length > 1 ? day : `0${day}`;
    hour = hour.toString().length > 1 ? hour : `0${hour}`;
    min = min.toString().length > 1 ? min : `0${min}`;
    sec = sec.toString().length > 1 ? sec : `0${sec}`;
    const _date = [year, month, day].join("-");
    const _time = [hour, min, sec].join(":");
    return `${_date} ${_time}`;
};

const genId = id => id ? id === 0 ? uv1() : id : uv1();

export const convert_to_new_data = (data, dt_time = null) => {
    if (isArray(data)) {
        if (data.length > 0) {
            data.forEach((v, i) => {
                v.id = genId(v.id);
                if (dt_time) {
                    v.created_at = v.created_at ? v.created_at : dt_time;
                    v.updated_at = dt_time;
                } else {
                    v.created_at = v.created_at ? v.created_at : get_created_at();
                    v.updated_at = get_created_at();
                }
                data[i] = v;
            });
        }
    } else if (isObject(data)) {
        data.id = genId(data.id);
        if (dt_time) {
            data.created_at = data.created_at ? data.created_at : dt_time;
            data.updated_at = dt_time;
        } else {
            data.created_at = data.created_at ? data.created_at : get_created_at();
            data.updated_at = get_created_at();
        }
    }
    return data;
};


export const global_bulk_insert = async options => {
    db.connection();
    const res = await db.insert({
        table: options.table,
        is_bulk: true,
        data_bulk: options.data
    });
    db.destory();
    if (res) {
        return ResponseSaveSuccess({message: options.res_ok});
    } else {
        return ResponseSaveError(options.res_err);
    }
};

export const mongodb_generate_order_by = (payload, default_key, default_sort) => {
    const sort = {};
    let checkSort = isNotNullAndNotEmpty(payload.sort);
    let checkOrder = isNotNullAndNotEmpty(payload.order);
    if (checkSort && checkOrder) {
        sort[payload.sort] = payload.order === "asc" ? 1 : -1;
    } else {
        sort[default_key] = default_sort;
    }
    return sort;
};
