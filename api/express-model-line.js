import db from './mongodb';
import {convert_to_new_data} from "./crud";

const defaultLineUser = {
    "id": 0,
    "line_display_image": "",
    "line_display_name": "",
    "line_user_id": "",
    "phone_number": "",
    "email": ""
};

class LineModel {
    constructor() {
        this.table_line = "user_line";
    }

    async line_save(payload, id) {
        try {
            payload = convert_to_new_data(payload);
            await db.connect();
            if (id) {
                delete payload["_id"];
                await db.database().collection(this.table_line).updateOne({_id: id}, {"$set": payload});
            } else {
                payload = {
                    ...defaultLineUser,
                    ...payload,
                    _id: payload["id"]
                };
                await db.database().collection(this.table_line).insertOne(payload);
            }
            await db.destroy();
            return payload;
        } catch (e) {
            console.log(e);
            return null;
        }
    }

    async count_line_by_mid(line_user_id) {
        try {
            await db.connect();
            const data = await db.database().collection(this.table_line)
                .find({line_user_id}).toArray();
            await db.destroy();
            return data;
        } catch (e) {
            console.log(e);
            return null;
        }
    }
}

export default new LineModel()