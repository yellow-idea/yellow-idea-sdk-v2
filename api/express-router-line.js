import express from 'express';
import axios from "axios";
import jwt from "jsonwebtoken";
import Model from "./express-model-line";

const qs = require("querystring");
const router = express.Router();

const client_id = process.env.LINE_CHANNEL_ID;
const client_secret = process.env.LINE_CHANNEL_SECRET;

async function fn_line_verify(access_token) {
    return await axios.get(`https://api.line.me/oauth2/v2.1/verify?access_token=${access_token}`);
}

async function fn_line_get_token(redirect_uri, code) {
    const payload = {
        grant_type: "authorization_code",
        code,
        redirect_uri,
        client_id: client_id,
        client_secret: client_secret
    };
    const headers = {
        headers: {
            'Content-Type': "application/x-www-form-urlencoded"
        },
    };
    return await axios.post("https://api.line.me/oauth2/v2.1/token", qs.stringify(payload), headers);
}

router.get('/liff', async (req, res) => {
    const {query} = req;
    // const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    let web = process.env.CLIENT_URL + '#/' + query.path;
    if (query.params) {
        web += '?' + query.params
    }
    // res.json({params, query, url: fullUrl, web});
    res.redirect(web);
    //window.location.href.replace(/(#access_token=.*)/, "")
    // const str = `<html><body><script>window.location.replace('${web}');</script></body></html>`;
    // res.send(str);
});

router.get('/login', async (req, res) => {
    const host = req.headers.host;
    const redirect_uri = req.protocol + "://" + host + "/" + process.env.ENV + req.baseUrl + '/callback';
    const state = "448a5532144-1460760543";
    const line_login_url = `https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id=${client_id}&redirect_uri=${redirect_uri}&scope=openid%20profile&state=${state}`;
    res.redirect(line_login_url);
    // res.json({line_login_url});
});

router.get('/callback', async (req, res) => {
    const {code, state} = req.query;
    // Check State
    if (!state) {
        res.json({error: 1, message: 'LINE State mismatched'});
        return false;
    }
    try {
        // Check Token
        const host = req.headers.host;
        const redirect_uri = req.protocol + "://" + host + "/" + process.env.ENV + req.baseUrl + '/callback';
        const tokenResponse = await fn_line_get_token(redirect_uri, code);
        if (tokenResponse.status !== 200) {
            res.json({error: 2, message: 'Token mismatched'});
            return false;
        }
        if (!tokenResponse.data.access_token) {
            res.json({error: 1001, message: 'Error! Code 1001'});
            return false;
        }

        // Access Token
        const res_token_data = await fn_line_verify(tokenResponse.data.access_token);
        if (res_token_data.status !== 200) {
            res.json({error: 5, message: 'Token mismatched'});
            return;
        }
        if (res_token_data.data.client_id !== client_id) {
            res.json({error: 6, message: 'Line Channel ID mismatched'});
            return;
        }
        const user_profile = jwt.decode(tokenResponse.data.id_token);
        const payload = {
            'line_user_id': user_profile['sub'],
            'line_display_name': user_profile['name'],
            'line_display_image': user_profile['picture'] || "",
        };
        const oldData = await Model.count_line_by_mid(payload.line_user_id);
        if (oldData.length <= 0) {
            const result = await Model.line_save(payload);
            // res.json(result);
            res.redirect(process.env.CLIENT_URL + "#/callback?uuid=" + result.id);
            return false;
        } else {
            res.redirect(process.env.CLIENT_URL + "#/callback?uuid=" + oldData[0].id);
            return false;
        }
    } catch (e) {
        res.json({error: 3, message: e.message || "-"});
    }
});

export default router;
