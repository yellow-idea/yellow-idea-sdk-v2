import fs from 'fs';
import json2xls from 'json2xls';
import {S3Upload} from './upload';
import { isNotNullAndNotEmpty } from "./checker";
require('dotenv').config({ path: './.env' })

export function transformJsonToExcel(data, options) {
    let _data = JSON.stringify(data);
    if (options) {
        if (options.replaceKeys) {
            if (options.replaceKeys.length > 0) {
                options.replaceKeys.forEach(v => {
                    _data = _data.replace(new RegExp(`"${v.key}"`, 'g'), `"${v.value}"`)
                });
            }
        }
    }
    return JSON.parse(_data);
}

export default function (key, fileName, data, obj) {
    return new Promise(async (resolve) => {
        try {
            const xls = json2xls(data);
            fileName = fileName + ".xlsx";
            if (obj) {
                const checkStartDate = isNotNullAndNotEmpty(obj.start_date);
                const checkEndDate = isNotNullAndNotEmpty(obj.end_date);
                if (checkStartDate && checkEndDate) {
                    fileName = `${fileName}-${obj.start_date}-${obj.end_date}.xlsx`;
                }
            }
            const tmpFile = '/tmp/' + fileName;
            fs.writeFileSync(tmpFile, xls, 'binary');
            const result = await S3Upload(process.env.S3, key + "/" + fileName, tmpFile);
            fs.unlinkSync(tmpFile);
            resolve({status: 'ok', url: result});
        } catch (e) {
            console.log(e);
            resolve({status: 'fail', url: 'upload fail'});
        }
    })
}
