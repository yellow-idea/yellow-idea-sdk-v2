const request = require("request");
require('dotenv').config({ path: './.env' })

export function errorNotify(fn, msg) {
    const message = `
Project  : ${process.env.LINE_NOTIFY_PROJECT_NAME}
Function : ${fn}
====================================
${msg}`;
    const options = {
        method: 'POST',
        url: 'https://notify-api.line.me/api/notify',
        headers:
            {
                Accept: '*/*',
                Authorization: 'Bearer oM14UdZdc5nhJbKzA2a84XcGhtUyT0A3yTwFqyKRD9B',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        form: {message}
    };
    console.log(message);
    request(options, function (error, response, body) {
        if (error) {
            console.log(error);
        }
    });
}
