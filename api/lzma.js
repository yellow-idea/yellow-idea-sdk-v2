import my_lzma from 'lzma';

function is_array(input) {
    return typeof (input) === "object" && (input instanceof Array);
}

function convert_to_formated_hex(byte_arr) {
    let hex_str = "",
        i,
        len,
        tmp_hex;

    if (!is_array(byte_arr)) {
        return false;
    }

    len = byte_arr.length;

    for (i = 0; i < len; ++i) {
        if (byte_arr[i] < 0) {
            byte_arr[i] = byte_arr[i] + 256;
        }
        if (byte_arr[i] === undefined) {
            alert("Boom " + i);
            byte_arr[i] = 0;
        }
        tmp_hex = byte_arr[i].toString(16);

        // Add leading zero.
        if (tmp_hex.length == 1) tmp_hex = "0" + tmp_hex;

        if ((i + 1) % 16 === 0) {
            tmp_hex += "\n";
        } else {
            tmp_hex += " ";
        }

        hex_str += tmp_hex;
    }

    return hex_str.trim();
}

function convert_formated_hex_to_bytes(hex_str) {
    var count = 0,
        hex_arr,
        hex_data = [],
        hex_len,
        i;

    if (hex_str.trim() == "") return [];

    /// Check for invalid hex characters.
    if (/[^0-9a-fA-F\s]/.test(hex_str)) {
        return false;
    }

    hex_arr = hex_str.split(/([0-9a-fA-F]+)/g);
    hex_len = hex_arr.length;

    for (i = 0; i < hex_len; ++i) {
        if (hex_arr[i].trim() == "") {
            continue;
        }
        hex_data[count++] = parseInt(hex_arr[i], 16);
    }

    return hex_data;
}

export const compress = data => {
    return new Promise((resolve, reject) => {
        my_lzma.compress(data, 1, function (result) {
            if (!result) {
                reject("An error occurred during compression.");
            }
            resolve(convert_to_formated_hex(result));
        });
    })
};

export const decompress = data => {
    return new Promise((resolve, reject) => {
        const byte_arr = convert_formated_hex_to_bytes(data);
        if (!byte_arr) {
            reject("invalid compressed input");
        }
        my_lzma.decompress(byte_arr, function (result) {
            if (!result) {
                reject("An error occurred during decompression.");
            }
            resolve(result);
        });
    });
};

export const compressJson = json => {
    return new Promise(async (resolve, reject) => {
        try {
            const data = await compress(JSON.stringify(json));
            resolve(data);
        } catch (e) {
            reject(e);
        }
    })
};

export const decompressJson = dataByte => {
    return new Promise(async (resolve, reject) => {
        try {
            const data = await decompress(dataByte);
            resolve(JSON.parse(data));
        } catch (e) {
            reject(e);
        }
    })
};

// async function testRun() {
//     const a = await compress(`ทดสอบ`);
//     const b = await decompress(a);
//     const aa = await compressJson({id: 1, name: "1234"});
//     const bb = await decompressJson(aa);
//     console.log({
//         a,
//         b,
//         aa,
//         bb
//     });
// }
