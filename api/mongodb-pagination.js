import db from "./mongodb";

export async function paginationAggregate(collection, query, sort, offset, limit, is_export = false) {
    let rows = [];
    let total = 0;
    try {
        await db.connect();
        if (!is_export) {
            total = await db.database().collection(collection)
                .aggregate([
                    ...query,
                    {
                        $count: "total"
                    }])
                .toArray();
            total = total.length > 0 ? total[0].total || 0 : 0;
        }
        rows = await db.database().collection(collection)
            .aggregate(query)
            .sort(sort)
            .skip(offset)
            .limit(limit)
            .toArray();
        await db.destroy();
        return {code_return: 1, rows, total};
    } catch (e) {
        console.log(e);
        return {code_return: -1, rows, total};
    }
}

export async function paginationFind(collection, query, sort, offset, limit, is_export = false) {
    let rows = [];
    let total = 0;
    try {
        await db.connect();
        if (!is_export) {
            total = await db.database().collection(collection)
                .find(query)
                .count();
        }
        rows = await db.database().collection(collection)
            .find(query)
            .sort(sort)
            .skip(offset)
            .limit(limit)
            .toArray();
        await db.destroy();
        return {code_return: 1, rows, total};
    } catch (e) {
        console.log(e);
        return {code_return: -1, rows, total};
    }
}
