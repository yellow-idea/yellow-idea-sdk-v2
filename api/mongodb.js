import mongodb from 'mongodb';
require('dotenv').config({ path: './.env' })

const {MongoClient} = mongodb;

class MongoDbHelper {
    async connect() {
        let host = process.env.DB_MONGODB;
        if (process.env.IS_LOCAL) {
            if (process.env.IS_LOCAL === "true") {
                host = process.env.DB_MONGODB_LOCAL;
            }
        }
        this.client = await MongoClient.connect(host, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        this.ObjectID = mongodb.ObjectID;
    }

    database() {
        return this.client.db(process.env.DB_MONGODB_DATABASE);
    }

    async destroy() {
        await this.client.close();
    }
}

export default new MongoDbHelper();
