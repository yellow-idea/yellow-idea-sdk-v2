import mongoose from 'mongoose';
require('dotenv').config({ path: './.env' })

export default async function () {
    let host = process.env.DB_MONGOOSE;
    if (process.env.IS_LOCAL && process.env.IS_LOCAL === "true") {
        host = process.env.DB_MONGOOSE_LOCAL;
    }

    mongoose.set('useCreateIndex', true);
    await mongoose.connect(host, {useNewUrlParser: true, useUnifiedTopology: true});
    return mongoose;
};
