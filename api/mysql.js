import mysql from 'mysql';
require('dotenv').config({ path: './.env' })
import {
    isArray,
    isObject,
    isHasKey
} from './checker';

const initInsert = {
    skip_keys: [],
    table: "",
    fields: {},
    is_bulk: false,
    data_bulk: []
};
const initUpdate = {
    skip_keys: [],
    table: "",
    fields: {},
    where: ""
};
const initDelete = {
    table: "",
    where: ""
};

function gen_value_insert_form_obj(obj, skip_keys = []) {
    let fieldsString = "";
    let fieldsValues = "";
    const keys = Object.keys(obj);
    keys.forEach((v, i) => {
        if (skip_keys.indexOf(v) === -1) {
            if (i === 0) {
                fieldsString += `\`${v}\``;
                fieldsValues += `'${obj[v]}'`;
            } else {
                fieldsString += `,\`${v}\``;
                fieldsValues += `,'${obj[v]}'`;
            }
        }
    });
    return {
        fieldsString,
        fieldsValues
    }
}

function gen_insert_query_by_obj(obj, table, skip_keys = []) {
    const {fieldsString, fieldsValues} = gen_value_insert_form_obj(obj, skip_keys);
    return `INSERT INTO ${table} (${fieldsString}) VALUES (${fieldsValues});`;
}

function gen_update_query_by_obj(obj, table, skip_keys = [], where = "") {
    let fieldsString = "";
    const keys = Object.keys(obj);
    keys.forEach((v, i) => {
        if (skip_keys.indexOf(v) === -1) {
            if (fieldsString === "") {
                fieldsString += `\`${v}\` = '${obj[v]}'`;
            } else {
                fieldsString += `,\`${v}\` = '${obj[v]}'`;
            }
        }
    });
    return `UPDATE ${table} SET ${fieldsString}${where};`;
}

class MysqlConnector {
    connection() {
        this.db = mysql.createConnection({
            connectionLimit: 1000,
            connectTimeout: 24 * 60 * 60 * 1000,
            acquireTimeout: 24 * 60 * 60 * 1000,
            timeout: 24 * 60 * 60 * 1000,
            multipleStatements: true,
            host: process.env.DB_HOST,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_DATABASE
        });
        this.db.connect();
    }

    query(sql) {
        return new Promise(((resolve, reject) => {
            try {
                this.db.query(sql, function (error, results, fields) {
                    if (error) {
                        console.log(error);
                    }
                    resolve(results);
                });
            } catch (e) {
                console.log(e);
            }
        }));
    }

    insert(options = initInsert) {
        let skip_keys = [];
        if (isHasKey(options, "skip_keys")) {
            skip_keys = options["skip_keys"]
        }
        return new Promise(((resolve, reject) => {
            let query = "";
            if (options.is_bulk) {
                options.data_bulk.forEach((v, i) => {
                    console.log(v);
                    const {fieldsString, fieldsValues} = gen_value_insert_form_obj(v, skip_keys);
                    if (i === 0) {
                        query = `INSERT INTO ${options.table} (${fieldsString}) VALUES (${fieldsValues})`;
                    } else {
                        query += `,(${fieldsValues})`
                    }
                })
            } else {
                const {fieldsString, fieldsValues} = gen_value_insert_form_obj(options.fields, skip_keys);
                query = `INSERT INTO ${options.table} (${fieldsString}) VALUES (${fieldsValues})`;
            }
            this.db.query(query, (error, results, fields) => {
                if (error) {
                    console.log(error);
                    reject(false);
                }
                resolve(true);
            });
        }));
    }

    update(options = initUpdate) {
        return new Promise(((resolve, reject) => {
            let fieldsString = "";
            const keys = Object.keys(options.fields);
            let skip_keys = [];
            if (isHasKey(options, "skip_keys")) {
                skip_keys = options["skip_keys"]
            }
            keys.forEach((v, i) => {
                if (skip_keys.indexOf(v) === -1) {
                    if (fieldsString === "") {
                        fieldsString += `\`${v}\` = '${options.fields[v]}'`;
                    } else {
                        fieldsString += `,\`${v}\` = '${options.fields[v]}'`;
                    }
                }
            });
            let query = `UPDATE ${options.table} SET ${fieldsString}${options.where}`;
            console.log(query);
            this.db.query(query, (error, results, fields) => {
                if (error) {
                    console.log(error);
                    reject(false);
                }
                resolve(true);
            });
        }));
    }

    delete(options = initDelete) {
        return new Promise(((resolve, reject) => {
            this.db.query(`DELETE FROM ${options.table}${options.where}`, (error, results, fields) => {
                if (error) {
                    console.log(error);
                    reject(false);
                }
                resolve(true);
            });
        }));
    }

    gen_dynamic_save(options) {
        let str_query = "";
        if (["INSERT_2D_ONE_OBJECT", "UPDATE_2D_ONE_OBJECT"].indexOf(options.type) > -1) {
            const processItems = () => {
                if (isHasKey(options, 'items_data')) {
                    if (options.items_data.length > 0) {
                        options.items_data.forEach(v => {
                            str_query += `DELETE FROM ${v.table} WHERE ${v.parent_field_name} = '${v.parent_id}';`;
                            if (isArray(v.data)) {
                                if (v.data.length > 0) {
                                    v.data.forEach((v2, i2) => {
                                        v2[v.parent_field_name] = v.parent_id;
                                        const {fieldsString, fieldsValues} = gen_value_insert_form_obj(v2, v.skip_keys);
                                        if (i2 === 0) {
                                            str_query += `INSERT INTO ${v.table} (${fieldsString}) VALUES (${fieldsValues})`;
                                        } else {
                                            str_query += `,(${fieldsValues})`;
                                        }
                                        if ((v.data.length - 1) === i2) {
                                            str_query += ";";
                                        }
                                    });
                                }
                            } else if (isObject(v.data)) {
                                str_query += gen_insert_query_by_obj(v.data, v.table);
                            }
                        })
                    }
                }
            };
            if (options.type === 'INSERT_2D_ONE_OBJECT') {
                str_query += gen_insert_query_by_obj(options.data, options.table, options.skip_keys);
                processItems();
            } else if (options.type === 'UPDATE_2D_ONE_OBJECT') {
                str_query += gen_update_query_by_obj(options.data, options.table, options.skip_keys, options.where);
                processItems();
            }
        }
        return str_query;
    }

    dynamic_save(options) {
        return new Promise((resolve, reject) => {
            const query = this.gen_dynamic_save(options);
            this.db.query(query, (error, results, fields) => {
                if (error) {
                    console.log("Save Error");
                    console.log(error);
                    reject(false);
                }
                resolve(true);
            });
        });
    }

    dynamic_merge_data(options) {
        if (options.data.length > 0) {
            if (options.items.length > 0) {
                options.items.forEach(v => {
                    options.data.forEach((v2, i2) => {
                        const parent_id = v2[v.source_field_name];
                        if (v.type === "ARRAY_OBJECT") {
                            const itemDataArrObject = v.data.filter(x => x[v.parent_field_name] === parent_id);
                            options.data[i2][v.key_name] = itemDataArrObject || [];
                        } else if (v.type === "PURE_OBJECT") {
                            const itemDataObject = v.data.find(x => x[v.parent_field_name] === parent_id);
                            options.data[i2][v.key_name] = itemDataObject || null;
                        } else if (v.type === "PURE_ARRAY") {
                            let arr = [];
                            if (v.data.length > 0) {
                                v.data.forEach(x => {
                                    if (x[v.parent_field_name] === parent_id) {
                                        arr.push(x[v.selected_field]);
                                    }
                                });
                            }
                            options.data[i2][v.key_name] = arr || [];
                        } else if (v.type === "JOIN_ARRAY_TO_STRING") {
                            let arr = [];
                            if (v.data.length > 0) {
                                v.data.forEach(x => {
                                    if (x[v.parent_field_name] === parent_id) {
                                        arr.push(x[v.selected_field]);
                                    }
                                });
                            }
                            options.data[i2][v.key_name] = arr.join(v.separator) || "";
                        }
                    });
                });
            }
        }
        return options.data;
    }

    destory() {
        this.db.end();
    }
}

export default new MysqlConnector();
