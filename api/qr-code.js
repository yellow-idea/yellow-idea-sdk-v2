import QRCode from 'qrcode';

export default function gen_qr_code(text) {
    return new Promise((resolve, reject) => {
        QRCode.toDataURL(text, function (err, url) {
            if (err) {
                console.log(err);
                reject(err);
            }
            resolve(url);
        });
    });
}
