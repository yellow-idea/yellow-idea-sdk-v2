export const Response200 = data => ({
    statusCode: 200,
    headers: {
        'Access-Control-Allow-Origin': "*",
        'Access-Control-Allow-Credentials': true
    },
    body: JSON.stringify(data, null, 2)
});

export const ResponseSaveSuccess = data => ({
    statusCode: 200,
    headers: {
        'Access-Control-Allow-Origin': "*",
        'Access-Control-Allow-Credentials': true
    },
    body: JSON.stringify({code_return: 1, ...data}, null, 2)
});

export const ResponseSaveError = message => ({
    statusCode: 200,
    headers: {
        'Access-Control-Allow-Origin': "*",
        'Access-Control-Allow-Credentials': true
    },
    body: JSON.stringify({code_return: -1, message}, null, 2)
});

export const Response500 = data => ({
    statusCode: 500,
    headers: {
        'Access-Control-Allow-Origin': "*",
        'Access-Control-Allow-Credentials': true
    },
    body: JSON.stringify({message: "ERROR_SOMETHING_WRONG"}, null, 2)
});
