export class MatchRouter {
    constructor(event) {
        this.path = event.path;
        const {httpMethod, queryStringParameters, headers, body} = event;
        this.payload = {
            path: event.path,
            headers,
            method: httpMethod,
            params: queryStringParameters,
            data: JSON.parse(body),
        };
    }

    match(pathName) {
        if (pathName === this.path.substr(0, pathName.length)) {
            this.payload = {
                subPath: this.path.substr(pathName.length),
                ...this.payload
            };
            return ["/", "?", ""].indexOf(this.path.substr(pathName.length, 1)) > -1;
        } else {
            return false;
        }
    }
}
