const initOptions = {"sort": "", "order": "asc", "limit": 10, "offset": 0};

export const sql_get_pagination = (payload = initOptions) => {
    let limit = '';
    if (payload.limit) {
        if (payload.offset) {
            limit = `LIMIT ${payload.offset} , ${payload.limit}`
        } else {
            limit = `LIMIT ${payload.limit}`
        }
    }
    let sort = '';
    if (payload.sort) {
        const order = payload.order || "";
        if (payload.sort !== "") {
            sort = `ORDER BY ${payload.sort} ${order.toUpperCase()}`;
        }
    }
    return {
        limit,
        sort
    }
};
