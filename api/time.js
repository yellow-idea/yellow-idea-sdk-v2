Date.prototype.addHours = function (h) {
    this.setHours(this.getHours() + h);
    return this;
};

export const get_date7 = () => new Date().addHours(7);

export const get_created_at = () => {
    const d = new Date().addHours(7);
    const year = d.getFullYear();
    let month = d.getMonth() + 1;
    let day = d.getDate();
    let hour = d.getHours();
    let min = d.getMinutes();
    let sec = d.getSeconds();
    month = month.toString().length > 1 ? month : `0${month}`;
    day = day.toString().length > 1 ? day : `0${day}`;
    hour = hour.toString().length > 1 ? hour : `0${hour}`;
    min = min.toString().length > 1 ? min : `0${min}`;
    sec = sec.toString().length > 1 ? sec : `0${sec}`;
    const _date = [year, month, day].join("-");
    const _time = [hour, min, sec].join(":");
    return `${_date} ${_time}`;
};
