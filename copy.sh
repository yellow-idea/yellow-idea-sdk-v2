#! /bin/sh

find . -name ".DS_Store" -print -delete
PATH_SOURCE=/Users/admin/Desktop/Javascript_Lib/yellow-idea-sdk/src # * = ALL
PATH_NODE=node_modules/yellow-idea-sdk
PATH_REACT=/Users/admin/Desktop/React-Dev
PATH_VUE=/Users/admin/Desktop/Vue_Dev

declare -a arrReact=(bhp-cms)
declare -a arrVue=(bhp-client app-uniliver)

for i in "${arrReact[@]}"
do
    cp -av $PATH_SOURCE $PATH_REACT/$i/$PATH_NODE
    rm -rf $PATH_REACT/$i/$PATH_NODE/node_modules
done

for i in "${arrVue[@]}"
do
    cp -av $PATH_SOURCE $PATH_VUE/2019/$i/$PATH_NODE
    rm -rf $PATH_VUE/2019/$i/$PATH_NODE/node_modules
done
