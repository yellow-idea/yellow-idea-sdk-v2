import axios from 'axios';
require('dotenv').config({ path: './.env' })

const baseURL = document.querySelector('meta[name=\'hostname\']') ? document.querySelector('meta[name=\'hostname\']').getAttribute('content') : process.env.VUE_APP_DEFAULT_API_URL;
const END_POINT = baseURL + '/api';

// function checkToken() {
//     const accessToken = getAccessToken().get('accessToken') || getFacebookToken();
//     const idToken = getIdToken().get('idToken') || getFacebookIDToken();
//     return {
//         accessToken,
//         idToken,
//     }
// }

export function httpRequest(method, url, data) {
    // let addOnHeader = {};
    //
    // const token = checkToken();
    //
    // if (token.accessToken) {
    //     addOnHeader = {
    //         ...addOnHeader,
    //         Authorization: token.accessToken,
    //     }
    // }
    //
    // if (token.idToken) {
    //     addOnHeader = {
    //         ...addOnHeader,
    //         'x-id-token': token.idToken,
    //     }
    // }
    let urlAxios = `${END_POINT}${url}`;
    if (url.search('http') > -1) {
        urlAxios = url;
    }

    const headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'user_id': 92,
        'yigenKey': 92
    };
    return axios({
        method,
        url: urlAxios,
        headers,
        data,
    })
        .then(response => Promise.resolve(response))
        .catch(error => Promise.reject(error.response.data));
}

export function httpRequestAws(method, url, data) {
    // let addOnHeader = {};
    //
    // const token = checkToken();
    //
    // if (token.accessToken) {
    //     addOnHeader = {
    //         ...addOnHeader,
    //         Authorization: token.accessToken,
    //     }
    // }
    //
    // if (token.idToken) {
    //     addOnHeader = {
    //         ...addOnHeader,
    //         'x-id-token': token.idToken,
    //     }
    // }
    let urlAxios = `${END_POINT}${url}`;
    if (url.search('http') > -1) {
        urlAxios = url;
    }

    const headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    };
    return axios({
        method,
        url: urlAxios,
        headers,
        data,
    })
        .then(response => Promise.resolve(response))
        .catch(error => Promise.reject(error.response.data));
}

export function httpRequestFirebase(method, url, data) {
    // let addOnHeader = {};
    //
    // const token = checkToken();
    //
    // if (token.accessToken) {
    //     addOnHeader = {
    //         ...addOnHeader,
    //         Authorization: token.accessToken,
    //     }
    // }
    //
    // if (token.idToken) {
    //     addOnHeader = {
    //         ...addOnHeader,
    //         'x-id-token': token.idToken,
    //     }
    // }
    let urlAxios = `${window.yellow_sdk_config.firebase_database_url}${url}`;

    const headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'user_id': 92,
        'yigenKey': 92
    };
    return axios({
        method,
        url: urlAxios,
        headers,
        data,
    })
        .then(response => Promise.resolve(response))
        .catch(error => Promise.reject(error.response.data));
}

export function httpRequestForm(method, url, data) {
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        'user_id': 92,
        'yigenKey': 92
    };
    let urlAxios = `${END_POINT}${url}`;
    if (url.search('http') > -1) {
        urlAxios = url;
    }
    return axios({
        method,
        url: urlAxios,
        headers,
        data,
    })
        .then(response => Promise.resolve(response))
        .catch(error => Promise.reject(error.response.data));
}

// const instanceForm = axios.create({
//     baseURL,
//     headers: {
//         'Content-Type': 'application/x-www-form-urlencoded',
//     },
// });

export const delay = ms => new Promise(resolve => setTimeout(() => resolve(true), ms));

