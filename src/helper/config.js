export default function (options) {
    window.yellow_sdk_config = {
        firebase_auth_domain: '',
        firebase_database_url: '',
        firebase_project_id: '',
        firebase_store_bucket: '',
        firebase_message_send_id: 0,
        ...options
    };
}
