import moment from 'moment';

export const getNowDatePicker = moment().format("YYYY-MM-DD");
export const toDatePicker = date => moment(date).format("YYYY-MM-DD");
export const toDateTimePicker = date => moment(date).format("YYYY-MM-DD HH:mm");
