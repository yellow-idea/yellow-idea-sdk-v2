export const CONVERT_FIREBASE_TO_REST = {
    toIndex: data => {
        if (data === null) {
            data = {};
        }
        let new_data = [];
        let newLoop = Object.keys(data);
        if (newLoop.length > 0) {
            newLoop.forEach((v, i) => {
                if (v !== null) {
                    data[v].id = v;
                    new_data.push(data[v]);
                }
            });
            data.datas = new_data;
            return {
                datas: new_data
            };
        } else {
            return {
                datas: new_data
            };
        }
    },
    toShow: (ids, data) => {
        data.id = ids;
        return {
            datas: {
                ...data
            }
        };
    },
    toAfterSave: status => {
        if (status === 200) return {
            code_return: 1
        };
        else return {
            code_return: 0
        };
    }
};
