import * as api from './api';
import config from './config';
import VueFilter from './vue-filter';
import * as firebase from './firebase';

const helper = {
    config,
    VueFilter,
    api,
    firebase
};

export default helper;
