import {toDateTimePicker} from './date-time';

export default function (Vue) {
    Vue.filter('toDateTime', value => toDateTimePicker(value));
    Vue.filter('toImageBlank', value => value ? value : './assets-yellow-v3/img/Noimage.png');
}
