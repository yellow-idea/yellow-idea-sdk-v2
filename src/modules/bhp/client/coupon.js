import {httpRequest, httpRequestFirebase} from '../../../helper/api';
import {getNowDatePicker} from '../../../helper/date-time';

export default new class Coupon {

    constructor() {
        this.http = httpRequestFirebase;
        this.url = "/api/bhp-2/coupon";
        this.data = {
            "id": 0,
            "name": "",
            "is_active": 1,
            "start_date": getNowDatePicker,
            "end_date": getNowDatePicker,
            // จำนวนครั้งที่สามารถกดใช้ได้
            "max_coupon": 1,
            "is_no_limit": 0,
            // ภาพตอนแสดง List
            "img_coupon_url": "",
            // ภาพตอนแสดง Detail
            "img_url_view": "",
            "tracking": {
                "id": 0,
                "l1": "-1",
                "l2": "-1",
                "l3": "-1",
                "cl1": "-1",
                "cl2": "-1",
                "cl3": "-1",
                "cl4": "-1",
                "cl5": "-1",
                "cl6": "-1",
                "cl7": "-1",
                "cl8": "-1",
                "desc": "",
                "code": "",
                "is_route_name": 0,
                "generated_full_url": "",
                "generated_short_url": ""
            },
            "message_share_friend": "",
            "url_share_friend": "",
            "is_enable_message_notification": 1,
            "message_notification_day_before_expire": 1,
            "message_notification_text": "",
            "message_notification_time": "00:00"
        };
    }

    // แสดงรายการ คูปอง
    getList() {
        return this.http('GET', this.url + '.json');
    }

    // แสดงรายละเอียด คูปอง
    getDetail(id) {
        return this.http('GET', `${this.url}/${id}.json`);
    }

    // ตรวจสอบจำนวนครั้งที่ยังสามารถกดใช้งานได้
    checkLimitRedeem() {

    }

    // กดใช้คูปอง
    submitRedeem() {

    }

    //Firebase
    async reset() {
        await this.http('DELETE', this.url + '.json');
        await this.http('POST', this.url + '.json', {
            ...this.data,
            "name": "Coupon Test 1",
            "img_url_view": "https://bhp-dev.yellow-idea.com/file_uploads/BHP/Image/12062019_015805-0.png",
            "img_coupon_url": "https://bhp-dev.yellow-idea.com/file_uploads/BHP/Image/12062019_015805-0.png",
        });
        await this.http('POST', this.url + '.json', {
            ...this.data,
            "name": "Coupon Test 2",
            "img_url_view": "https://bhp-dev.yellow-idea.com/file_uploads/BHP/Image/12062019_015952-0.png",
            "img_coupon_url": "https://bhp-dev.yellow-idea.com/file_uploads/BHP/Image/12062019_015952-0.png",
        });
    }
}();
