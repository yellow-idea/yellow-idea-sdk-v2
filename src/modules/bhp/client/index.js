import Coupon from './coupon';
import Sampling from './sampling';
import DynamicContent from './questionnare';
import Questionnaire from './dynamic-content';

const BhpClient = {
    Coupon,
    Sampling,
    DynamicContent,
    Questionnaire
};

export default BhpClient;
