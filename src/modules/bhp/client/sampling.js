import {httpRequest, httpRequestFirebase} from '../../../helper/api';
import {getNowDatePicker} from '../../../helper/date-time';

export default new class Sampling {

    constructor() {
        this.http = httpRequestFirebase;
        this.url = "/api/bhp-2/sampling";
        this.data = {
            "id": 0,
            "name": "",
            "alt_text": "",
            "is_active": 1,
            "start_date": getNowDatePicker,
            "end_date": getNowDatePicker,
            "max_sampling": 1,
            "stock_sampling": 0,
            "is_no_limit": 0,
            // ภาพตอนแสดง List
            "img_main_url": "",
            // ภาพตอนแสดง Detail
            "img_url_view": "",
            "is_enable_message_notification": 1,
            "message_notification_day_before_expire": 1,
            "message_notification_text": "",
            "message_notification_time": "00:00",
            "message_share_friend": "",
            "url_share_friend": "",
            "tracking": {
                "id": 0,
                "l1": "-1",
                "l2": "-1",
                "l3": "-1",
                "cl1": "-1",
                "cl2": "-1",
                "cl3": "-1",
                "cl4": "-1",
                "cl5": "-1",
                "cl6": "-1",
                "cl7": "-1",
                "cl8": "-1",
                "desc": "",
                "code": "",
                "is_route_name": 0,
                "generated_full_url": "",
                "generated_short_url": ""
            },
            "qr_height": 100,
            "qr_width": 100,
            "qr_x": 0,
            "qr_y": 0
        };
    }

    getList() {
        return this.http('GET', this.url + '.json');
    }

    getDetail(id) {
        return this.http('GET', `${this.url}/${id}.json`);
    }

    //Firebase
    async reset() {
        await this.http('DELETE', this.url + '.json');
        await this.http('POST', this.url + '.json', {
            ...this.data,
            "name": "Coupon Test 1",
            "img_url_view": "https://bhp-dev.yellow-idea.com/file_uploads/BHP/Image/12062019_015805-0.png",
            "img_main_url": "https://bhp-dev.yellow-idea.com/file_uploads/BHP/Image/12062019_015805-0.png",
        });
        await this.http('POST', this.url + '.json', {
            ...this.data,
            "name": "Coupon Test 2",
            "img_url_view": "https://bhp-dev.yellow-idea.com/file_uploads/BHP/Image/12062019_015952-0.png",
            "img_main_url": "https://bhp-dev.yellow-idea.com/file_uploads/BHP/Image/12062019_015952-0.png",
        });
    }
}();
