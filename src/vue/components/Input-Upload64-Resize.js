export default {
    template: `<input type="file" ref="inputRef" style="display: none" accept="image/*" @change="onUpload"/>`,
    props: ['max_size'],
    methods: {
        onClicked: function () {
            this.$refs.inputRef.click();
        },
        getBase64(file, max_size) {
            return new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.onload = function (readerEvent) {
                    const image = new Image();
                    image.onload = function () {
                        let w = image.width;
                        let h = image.height;

                        if (w > h) {
                            if (w > max_size) {
                                h *= max_size / w;
                                w = max_size;
                            }
                        } else {
                            if (h > max_size) {
                                w *= max_size / h;
                                h = max_size;
                            }
                        }

                        const canvas = document.createElement('canvas');
                        canvas.width = w;
                        canvas.height = h;
                        canvas.getContext('2d').drawImage(image, 0, 0, w, h);

                        let dataURL = '';
                        if (file.type === 'image/jpeg') {
                            dataURL = canvas.toDataURL('image/jpeg', 1.0);
                        } else {
                            dataURL = canvas.toDataURL('image/png');
                        }
                        resolve(dataURL);
                    };
                    image.src = readerEvent.target.result;
                };
                reader.onerror = error => reject(error);
                reader.readAsDataURL(file);
            });
        },
        async onUpload(e) {
            if (e.currentTarget.files[0]) {
                const data = await this.getBase64(e.currentTarget.files[0], parseFloat(this.max_size) || 300);
                this.$refs.inputRef.value = '';
                this.$emit('onUpload', data);
            }
        },
    }
};
