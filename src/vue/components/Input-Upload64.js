export default {
    template: `<input type="file" ref="inputRef" style="display: none" accept="image/*" @change="onUpload"/>`,
    methods: {
        onClicked: function () {
            this.$refs.inputRef.click();
        },
        getBase64(file) {
            return new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = () => resolve(reader.result);
                reader.onerror = error => reject(error);
            });
        },
        async onUpload(e) {
            if (e.currentTarget.files[0]) {
                const data = await this.getBase64(e.currentTarget.files[0]);
                this.$refs.inputRef.value = '';
                this.$emit('onUpload', data);
            }
        },
    }
};
