export default {
    props: ['color'],
    computed: {
        style: function () {
            const c = this.color || '#3498db';
            return `border-color: ${c} transparent transparent transparent;`;
        }
    },
    template: `<div style="text-align: center;padding:15% 0;">
        <div class="lds-ring" :style="style">
            <div :style="style"></div>
            <div :style="style"></div>
            <div :style="style"></div>
            <div :style="style"></div>
        </div>
    </div>`
};
