import {httpRequestAws} from '../../helper/api';

export default {
    data() {
        return {
            env: window.yellow_sdk_config,
            isLoading: true,
            isLoadingSave: false,
            line_user_id: document.getElementById('xooFdswG14') ? document.getElementById('xooFdswG14').value : '',
            errorMessages: [],
            http: null
        };
    },
    created() {
        const api_url = this.env.AWS_API;
        this.http = {
            get: url => httpRequestAws('GET', api_url + url),
            post: (url, payload) => httpRequestAws('POST', api_url + url, payload),
            put: (url, payload) => httpRequestAws('PUT', api_url + url, payload),
            delete: (url) => httpRequestAws('DELETE', api_url + url),
        };
    },
    mounted() {
        if (this.line_user_id !== '') {
            if (this.line_user_id === '0') {
                this.$router.push('/404');
            } else {
                if (this.onLoad) {
                    this.onLoad();
                }
            }
        }
    }
};
